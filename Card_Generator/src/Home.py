from PIL import Image,ImageTk
import tkinter as tk
import os
from tkinter import ttk,messagebox


class CardGeneratorApp :
    def __init__(self):
        self.c2w_root=tk.Tk()
        self.c2w_screen_width=self.c2w_root.winfo_screenwidth()
        self.c2w_screen_height=self.c2w_root.winfo_screenheight()
        self.c2w_root.title("Tkinter Card Generator")
        self.c2w_root.geometry(f"{self.c2w_screen_width}x{self.c2w_screen_height}")
        self.c2w_root.configure(bg="#FFFFFF")

        self.setup_ui()

    def setup_ui(self):
        self.c2w_background_frame=tk.Frame(self.c2w_root,bg="#0A2472",width=self.c2w_screen_width,height=self.c2w_screen_height)
        self.c2w_background_frame.pack()

        self.c2w_root=tk.Label(self.c2w_root,text="Enter Your Details",font=("calibre",30,"bold"),bg="azure2").place(x=100,y=10)


    

    def run(self):
        self.c2w_root.mainloop()
