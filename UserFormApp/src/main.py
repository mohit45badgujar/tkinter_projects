import tkinter as tk


# Create the main window
root = tk.Tk()
root.geometry('2000x2000')
root.title("User Profile Form")
#root.configure(bg='blue')  # Set background color

# Create labels and entry fields
label_0 = tk.Label(root, text="User Profile Form", width=30, font=("bold", 20))
label_0.place(x=450, y=53)
label_0.configure(bg='blue',fg='white')


label_1 = tk.Label(root, text="Enter First Name :", width=20, font=("bold", 12))
label_1.place(x=430, y=130)
label_1.configure(width=20)
label_1.configure(height=1)
entry_1 = tk.Entry(root)
entry_1.place(x=620, y=130)
entry_1.configure(width=50)

label_2 = tk.Label(root, text="Enter Last Name :", width=20, font=("bold", 12))
label_2.place(x=430, y=180)
label_2.configure(width=20)
#label_2.configure(height=1)
entry_2 = tk.Entry(root)
entry_2.place(x=620, y=180)
entry_2.configure(width=50)


label_3 = tk.Label(root, text="Enter Mobile No :", width=20, font=("bold", 12))
label_3.place(x=430, y=230)
label_3.configure(width=20)
entry_3 = tk.Entry(root)  # Corrected duplicate entry name
entry_3.place(x=620, y=230)
entry_3.configure(width=50)

label_4=tk.Label(root,text="Select Profile Photo :",width=20,font=("bold",12))
label_4.place(x=430,y=280)
#entry_4=tk.Entry(root)
label_5=tk.Label(root,text="No File Selected ",width=20,font=("bold",12))
label_5.place(x=620,y=280)
#label_5.configure(width=50)


photo_button = tk.Button(root, text='Select Photo', width=52, bg='yellow', fg='black',font=12)
photo_button.place(x=450, y=330)

label_6 = tk.Label(root, text="Gender", width=20, font=("bold", 12))
label_6.place(x=430, y=380)
gender_var = tk.IntVar()  # Use tk.IntVar for radio buttons
radio_male = tk.Radiobutton(root, text="Male", padx=5, variable=gender_var, value=1,font=12)
radio_male.place(x=620, y=380)
radio_female = tk.Radiobutton(root, text="Female", padx=5, variable=gender_var, value=2,font=12)
radio_female.place(x=620, y=410)

label_7 = tk.Label(root, text="Height(cm) :", width=20, font=("bold", 12))
label_7.place(x=430, y=460)
entry_7 = tk.Entry(root)  
entry_7.place(x=620, y=460)
entry_7.configure(width=50)

save_button = tk.Button(root, text='Save', width=52, bg='brown', fg='white',font=12)
save_button.place(x=450, y=510)

save_button = tk.Button(root, text='Save', width=52, bg='orange', fg='black',font=12)
save_button.place(x=450, y=510)

record_button = tk.Button(root, text=' View All Records ', width=52, bg='pink', fg='black',font=12)
record_button.place(x=450, y=560)
# Start the main event loop
root.mainloop()